import 'package:flutter/material.dart';
import 'package:blue52/components/models.dart';
import 'package:blue52/components/photo_scroller.dart';
import 'package:blue52/components/theme.dart';
import 'package:blue52/components/post_model.dart';
import 'package:blue52/components/post.dart';

class Home extends StatefulWidget {
  Home(this.content);
  final Content content;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<PostModal> items = new List();

  @override
  void initState() {
    setState(() {
      items.add(new PostModal(
        1,
        2,
        'Silicon Valley',
        'Launching the apps',
        'Feel so great',
      ));
      items.add(new PostModal(
        1,
        3,
        'Silicon Valley',
        'Launching the apps',
        'Feel so great',
      ));
      items.add(new PostModal(
        2,
        4,
        'Silicon Valley',
        'Launching the apps',
        'Feel so great',
      ));
      items.add(new PostModal(
        2,
        5,
        'Silicon Valley',
        'Launching the apps',
        'Feel so great',
      ));
      items.add(new PostModal(
        3,
        6,
        'Silicon Valley',
        'Launching the apps',
        'Feel so great',
      ));
      items.add(new PostModal(
        3,
        7,
        'Silicon Valley',
        'Launching the apps',
        'Feel so great',
      ));
      items.add(new PostModal(
        3,
        8,
        'Silicon Valley',
        'Launching the apps',
        'Feel so great',
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    // var textTheme = Theme.of(context).textTheme;

    return Scaffold(
      body: Stack(children: <Widget>[
        ClipPath(
          child: new Container(
              height: MediaQuery.of(context).size.height * 0.25,
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                    colors: [Colors.blue, Colors.lightBlueAccent],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.0, 0.0),
                    stops: [0.0, 1.2],
                    tileMode: TileMode.clamp),
              )),
          clipper: HeaderColor(),
        ),
        //         SafeArea(
        //   child: Container(
        //     alignment: Alignment.topCenter,
        //       // padding: EdgeInsets.symmetric(vertical: 20.0),
        //       child: Image.asset(
        //         'assets/images/whale.png',
        //         height: 50.0,
        //       )),
        // ),
        SafeArea(
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                    alignment: Alignment.topCenter,
                    // padding: EdgeInsets.symmetric(vertical: 20.0),
                    child: Image.asset(
                      'assets/images/whale.png',
                      height: 90.0,
                    )),
              ),
              Expanded(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Card(
                    elevation: 5.0,
                    shape: RoundedRectangleBorder(
                      side: BorderSide.none,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(0.0),
                          child: const ListTile(
                            leading: Icon(
                              Icons.person_pin_circle,
                              size: 50.0,
                            ),
                            title: Text('What\'s your planning ?',
                                style:
                                    TextStyle(color: Color(Pallete.theGrey))),
                            subtitle: Text("Caption",
                                style:
                                    TextStyle(color: Color(Pallete.theGrey))),
                            trailing: InkResponse(
                                child: Icon(Icons.send), onTap: null),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 6,
                child: Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Post(items),
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}

// child: SingleChildScrollView(
//           child: Column(
//             children: [
//               Text(
//                 'Tops',
//                 style: textTheme.subhead.copyWith(fontSize: 18.0),
//               ),
//               PhotoScroller(content.photoUrls),
//               SizedBox(height: 20.0),
//               Text(
//                 'Bottom',
//                 style: textTheme.subhead.copyWith(fontSize: 18.0),
//               ),
//               PhotoScroller(content.photoUrls),
//               SizedBox(height: 20.0),
//               Text(
//                 'Shoes',
//                 style: textTheme.subhead.copyWith(fontSize: 18.0),
//               ),
//               PhotoScroller(content.photoUrls),
//               SizedBox(height: 20.0),
//               Text(
//                 'Accessories',
//                 style: textTheme.subhead.copyWith(fontSize: 18.0),
//               ),
//               PhotoScroller(content.photoUrls),
//               SizedBox(height: 20.0),
//             ],
//           ),
//         ),

class HeaderColor extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();

    path.lineTo(0.0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
