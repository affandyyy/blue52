import 'package:flutter/material.dart';
import 'package:blue52/components/theme.dart';

import 'package:blue52/components/post_model.dart';

class Post extends StatelessWidget {
  Post(this.items);

  List<PostModal> items = new List();

  Widget _postingBar(BuildContext context, int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Card(
          shape: RoundedRectangleBorder(
            side: BorderSide.none,
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Column(
            children: <Widget>[
              Divider(height: 5.0),
              ListTile(
                title: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '${items[index].title}',
                      style: TextStyle(
                        fontSize: 10.0,
                        color: Colors.blue,
                      ),
                    ),
                    Text(
                      '${items[index].body}',
                      overflow: TextOverflow.ellipsis,
                      style: new TextStyle(color: Color(Pallete.theGrey)
                          // fontSize: 18.0,
                          // fontStyle: FontStyle.italic,
                          ),
                    ),
                    Text(
                      '${items[index].caption}',
                      style: new TextStyle(
                        color: Color(Pallete.theGrey),
                        // fontSize: 18.0,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                  ],
                ),
                // subtitle:
                leading: Column(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.blueAccent,
                      radius: 25.0,
                      child: Text(
                        'User ${items[index].userId}',
                        style: TextStyle(
                          // fontSize: 22.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    // IconButton(
                    //   icon: const Icon(Icons.remove_circle_outline),
                    //   onPressed: () {
                    //   },
                    // ),
                  ],
                ),
                trailing: IconButton(
                  icon: const Icon(Icons.remove_circle_outline),
                  onPressed: () {},
                ),

                // onTap: () => _onTapItem(context, items[position]),
              ),
            ],
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: _postingBar,
      itemCount: items.length,
    );
  }
}
