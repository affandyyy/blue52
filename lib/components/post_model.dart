class PostModal {
  final int userId;
  final int id;
  final String title;
  final String body;
  final String caption;
 
  PostModal(this.userId, this.id, this.title, this.body, this.caption);
}