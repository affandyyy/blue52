import 'package:blue52/components/models.dart';

final Content testMovie = Content(
  bannerUrl: 'assets/images/banner.png',
  posterUrl: 'assets/images/poster.png',
  title: 'The Secret Life of Pets',
  rating: 8.0,
  starRating: 4,
  categories: ['Animation', 'Comedy'],
  storyline: 'For their fifth fully-animated feature-film '
      'collaboration, Illumination Entertainment and Universal '
      'Pictures present The Secret Life of Pets, a comedy about '
      'the lives our...',
  photoUrls: [
    'assets/images/1.png',
    'assets/images/2.png',
    'assets/images/3.png',
    'assets/images/4.png',
    'assets/images/1.png',
  ],
  namaKedai:[
    'Ali Cendol',
    'Bistro Q',
    'Warung Bunian',
    'ABC Power',
    'Laksa Johor'
  ]
);
