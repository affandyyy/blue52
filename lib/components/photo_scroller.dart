import 'package:flutter/material.dart';

class PhotoScroller extends StatelessWidget {
  PhotoScroller(this.photoUrls, this.namaKedai);
  final List<String> photoUrls;
  final List<String> namaKedai;

  Widget _buildPhoto(BuildContext context, int index) {
    var photo = photoUrls[index];
    var nama = namaKedai[index];

    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 15.0),
          child: Card(
            elevation: 5.0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(4.0),
              child: Image.asset(
                photo,
                width: 120.0,
                height: 140.0,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Text(
            nama,
            style: TextStyle(fontWeight: FontWeight.w200),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // var textTheme = Theme.of(context).textTheme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Padding(
        //   padding: const EdgeInsets.symmetric(horizontal: 20.0),
        //   child: Text(
        //     'Photos',
        //     style: textTheme.subhead.copyWith(fontSize: 18.0),
        //   ),
        // ),
        SizedBox.fromSize(
          size: const Size.fromHeight(190.0),
          child: ListView.builder(
            itemCount: photoUrls.length,
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.only(top: 8.0, left: 20.0),
            itemBuilder: _buildPhoto,
          ),
        ),
      ],
    );
  }
}
